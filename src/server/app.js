import fs from 'fs'
import path from 'path'
import express from 'express'
import React from 'react'
import bodyParser from 'body-parser'
import {renderToString} from 'react-dom/server'
import request from 'request'

import HelloWorld from '../client/components/helloWorld'

const app = express()
app.use(bodyParser.json())

function sendMessage (recipientId, message) {
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
      access_token: process.env.PAGE_ACCESS_TOKEN
    },
    method: 'POST',
    json: {
      recipient: {
        id: recipientId
      },
      message: message
    }
  }, function (error, response, body) {
    if (error) {
      console.log('Error sending message: ' + response.error)
    }
  })
}

app.get('/', (req, res) => {
  // Loads a template.
  const pathToHtml = path.join(__dirname, './views/index.html')
  const template = fs.readFileSync(pathToHtml, 'utf8')

  // Inserts a rendered react component to the loaded template (server-side
  // rendering).
  const renderedHelloWorld = renderToString(<HelloWorld />)
  const page = template.replace('<!-- CONTENT -->', renderedHelloWorld)

  res
    .status(200)
    .send(page)
})

// Opens a socket and listens for connections only if there is no parent module
// running the script.

if (!module.parent) {
  let PORT = process.env.PORT || 8080
  app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
  })
}

app.post('/webhook', (req, res) => {
  let body = req.body

  // Check the webhook event is from a Page subscription
  if (body.object === 'page') {
    body
      .entry
      .forEach(function (entry) {
        let webHookEvent = entry.messaging[0]
        console.log(webHookEvent)

        let text = webHookEvent.message.text

        request({
          url: 'https://graph.facebook.com/v2.6/' + webHookEvent.sender.id,
          qs: {
            access_token: process.env.PAGE_ACCESS_TOKEN,
            fields: 'first_name'
          },
          method: 'GET'
        }, function (error, response, body) {
          if (error) {
            console.log("Error getting user's name: " + error)
          } else {
            let bodyObj = JSON.parse(body)
            let name = bodyObj.first_name
            let message = ''

            request({
              url: `https://api.wit.ai/message?v=20170307&q=${text}?`,
              headers: {
                'Authorization': `Bearer ${process.env.WIT_TOKEN}`
              },
              method: 'GET'
            }, function (error, response, witBodyString) {
              if (error) {
                console.log('Error sending message: ' + response.error)
              }
              let witBody = JSON.parse(witBodyString)

              if (witBody && witBody.entities) {
                let numberEntity = witBody.entities.number
                if (numberEntity && numberEntity.length > 0) {
                  let amount = numberEntity[0].value
                  if (amount < 5000) {
                    message = `Yes ${name}!, You can borrow ${amount} pesos`
                  } else {
                    message = `${name} Sorry, but we cannot lend you ${amount} pesos`
                  }
                } else {
                  message = `Hi ${name}, Please specify the amount that you want to borrow.`
                }
                sendMessage(webHookEvent.sender.id, {text: message})
              }
            })
          }
        })
      })

    // Return a '200 OK' response to all events
    res
      .status(200)
      .send('EVENT_RECEIVED')
  } else {
    res.sendStatus(404)
  }
})

// Accepts GET requests at the /webhook endpoint
app.get('/webhook', (req, res) => {
  const VERIFY_TOKEN = 'verifytoken'

  // Parse params from the webhook verification request
  let mode = req.query['hub.mode']
  let token = req.query['hub.verify_token']
  let challenge = req.query['hub.challenge']
  console.log(mode, token, challenge, req.query)
  // Check if a token and mode were sent
  if (mode && token) {
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      console.log('WEBHOOK_VERIFIED')
      res
        .status(200)
        .send(challenge)
    } else {
      res.sendStatus(403)
    }
  }
})

export default app
