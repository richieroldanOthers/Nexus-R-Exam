import request from 'supertest'

import app from '../app'

describe('App', () => {
  it('renders HelloWorld component', async () => {
    const res = await request(app).get('/')

    expect(res.statusCode).toBe(200)
    expect(res.text).toContain('class="hello-world"')
  })
})

describe('App', () => {
  it('renders HelloWorld component', async () => {
    const res = await request(app).get('/')

    expect(res.statusCode).toBe(200)
    expect(res.text).toContain('class="hello-world"')
  })
})

describe('Webhook Post', () => {
  it('get information from facebook', async () => {
    const res = await request(app).post('/webhook').send({ 'object': 'page',
      'entry': [{'time': 1458692752478, 'messaging': [{'sender': {'id': 'testID'}, 'recipient': {'id': 'testID'}, 'message': {'text': 'test'}}]}
      ]})

    expect(res.statusCode).toBe(200)
    expect(res.text).toContain('EVENT_RECEIVED')
  })

  it('get information from facebook with error', async () => {
    const res = await request(app).post('/webhook')

    expect(res.statusCode).toBe(404)
  })
})

describe('Webhook Get', () => {
  it('Facebook Verification', async () => {
    const res = await request(app).get('/webhook?hub.verify_token=verifytoken&hub.mode=subscribe&hub.challenge=testChallenge')

    expect(res.statusCode).toBe(200)
    expect(res.text).toContain('testChallenge')
  })
})
