import React from 'react'
import { connect } from 'react-redux'
import { store, changeMessage } from '../redux'

export function HelloWorld (props) {
  return (
    <div className='hello-world'>
      <h1>Hello world!</h1>
      <p> Nexus R chatbot is now online. To test it, please message this page <a href='https://www.facebook.com/Nexus-R-Exam-151023782256027/'>Nexus R Exam Page</a> </p>
    </div>
  )
}

// REACT-REDUX
// pass down responsive store state as props
const mapStateToProps = state => ({})

// pass down dispatchers as props
const mapDispatchToProps = dispatch => ({changeMessage: txt => dispatch(changeMessage(txt))})

// hook up props with base component
const ConnectedWorld = connect(mapStateToProps, mapDispatchToProps)(HelloWorld)

// pass down store obj as direct props
const App = () => <ConnectedWorld store={store} />

export default App
