# Spiralworks Test

## Requirements : 

### Objectives
- Create a working prototype
- Earn experience in making chatbots in less than 1 and a half hour!
- Add another Repo in your Git Repository
### Milestones
- Create a Hello World App
- Create a function that will accept `Can I loan 5000 pesos?` and return Yes otherwise,
no
- Create a chatbot and integrate it with FB page
- Deploy to Heroku

### Bonus
- Integrate wit.ai to the client app
    - Conditions
        - If the user has an intent to borrow and the sentence has an amount, Return `Yes` or `No`
        - `Yes` if amount is less than 5000; else `No`
- Dockerize
- Use TDD
- Use proper file structure

### Hints
- Ngrok
- Regexp

### Rules
- Use proper file structure
- Boilerplates are allowed
- Commit frequently

## URL 
https://nexus-r-exam.herokuapp.com/

## FB Page
https://www.facebook.com/Nexus-R-Exam-151023782256027/
#### Note : ***Please send me the fb usernames of the testers***

### Getting Started

- Install NodeJS (https://nodejs.org/en/download/)
- Clone the Project
    ```
    $ git clone https://gitlab.com/richieroldanOthers/Nexus-R-Exam.git
    ```
- Install Dependencies
    ```
    $ cd Nexus-R-Exam
    $ yarn
    ```
- Run the app
     ```
    $ yarn dev
    ```
- Run test
     ```
    $ yarn test
    ```


### Framework/Libraries/Plugins
- NodeJS
- ReactJS
- Express JS
- Jest
- StandardJS

### Services Used

- wit.ai - ai that identify and evaluate user's asking money
- FB messager - FB Chatbot
    - Note : ***Please send me the fb usernames of the testers***

### Deployment
- I used Heroku to deploy the app (https://www.heroku.com/). 
- I'm using Docker
- I am the one who has the account therefore I'll be the one who can publish it.
- Auto deployment is already configured
- Auto deployment is triggered when a user ***heroku container:push web*** using heroku cli
- Heroku git repo : https://git.heroku.com/nexus-r-exam.git


**If you have questions and clarification Please contact me. Thank you!**

## &copy; Richie Roldan